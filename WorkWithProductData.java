package TechportTestTasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WorkWithProductData {
    private static Random random = new Random();

    public static void main(String[] args) throws InterruptedException {

        //Тестовая база
        List<Product> testDataBase = new ArrayList<>();
        testDataBase.add(new Product(36470, 12400, 590, "Холодильник Атлант 4008-022", true));
        testDataBase.add(new Product(26109, 20290, 590, "Холодильник Атлант 6025-031", true));
        testDataBase.add(new Product(46245, 14240, 490, "Холодильник Атлант 4010-022", false));
        testDataBase.add(new Product(34126, 12100, 590, "Холодильник Атлант ХМ 4208-000", true));
        testDataBase.add(new Product(26661, 20780, 490, "Холодильник Атлант 6026-031", false));
        testDataBase.add(new Product(32034, 13260, 590, "Холодильник Атлант 2835-90", true));
        testDataBase.add(new Product(38031, 17838, 590, "Холодильник Атлант 6023-031", true));

        class PriceThread extends Thread {
            Thread t;
            int number;

            PriceThread(int number) throws InterruptedException {
                this.number = number;
                t = new Thread(this);
                t.start();
                t.join();
            }

            public void run() {
                try {
                    int time = random.nextInt(5000);
                    if (time > 4000) {
                        System.out.print(" " + Thread.currentThread() + "  end");
                        return;
                    }
                    Thread.sleep(time);
                    getPriceFromLst(testDataBase);
                } catch (InterruptedException e) {
                    System.out.print("Error message: " + e.getMessage());
                }
            }

            private int getPriceFromLst(List<Product> price) {
                System.out.print(price.get(number).getPrice());
                return price.get(number).getPrice();
            }
        }

        class DeliveryPriceThread extends Thread  {
            Thread t;
            int number;

            DeliveryPriceThread(int number) throws InterruptedException {
                this.number = number;
                t = new Thread(this);
                t.start();
                t.join();
            }

            public void run() {
                try {
                    int time = random.nextInt(5000);
                    if (time > 4000) {
                        System.out.print(" " + Thread.currentThread() + "  end");
                        return;
                    }
                    Thread.sleep(time);
                    getDeliveryPriceFromList(testDataBase);
                } catch (InterruptedException e) {
                    System.out.print("Error message: " + e.getMessage());
                }
            }

            private int getDeliveryPriceFromList(List<Product> price) {
                System.out.print(" " + price.get(number).getDeliveryPrice() + " ");
                return price.get(number).getDeliveryPrice();
            }
        }

        class AvailableThread extends Thread  {
            Thread t;
            int number;

            AvailableThread(int number) throws InterruptedException {
                this.number = number;
                t = new Thread(this);
                t.start();
                t.join();
            }

            public void run() {
                try {
                    int time = random.nextInt(5000);
                    if (time > 4000) {
                        System.out.print(" " + Thread.currentThread() + "  end");
                        return;
                    }
                    Thread.sleep(time);
                    getIsAviable(testDataBase);
                } catch (InterruptedException e) {
                    System.out.print("Error message: " + e.getMessage());
                }
            }

            private boolean getIsAviable(List<Product> price) {
                System.out.print(" " + price.get(number).isAviability());
                return price.get(number).isAviability();
            }
        }

        for (int i = 0; i < testDataBase.size(); i++) {
            PriceThread priceThread = new PriceThread(i);
            DeliveryPriceThread deliveryPriceThread = new DeliveryPriceThread(i);
            AvailableThread availableThread = new AvailableThread(i);
            priceThread.join();
            deliveryPriceThread.join();
            availableThread.join();

            System.out.println();
            System.out.println("================"); //Разделение на строки
        }
    }
}